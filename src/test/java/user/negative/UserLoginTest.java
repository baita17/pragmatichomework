package user.negative;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import utils.Browser;
import utils.CommongVerifications;

public class UserLoginTest {

    @DataProvider
    public Object[][] testData() {
        return new String[][] {
                new String[] {"tania_anq@abv.bg","123"},
                new String[] {"tania_anq1@abv.bg","invalid"},
                new String[] {"tania_anq312321@abv.bg","#123456?"},
        };
    }


    @Test
    public static void userLoginTest() {
        HomePage.openHomePage();
        CommongVerifications.verifyTitle("Pragmatic Test Store", "Homepage not loaded");
        HomePage.goToLogin();
        CommongVerifications.verifyTitle("Account Login", "Login page isn't loaded");
        LoginPage.login("tania_anq@abv.bg", "123");
        LoginPage.verifyErrorValidationMessage("Warning: No match for E-Mail Address and/or Password.", "The expected validation error message is INCORRECT!");

    }

    @Test(dataProvider = "testData")
    public static void userLoginTestDDD(String email, String password) {
        HomePage.openHomePage();
        CommongVerifications.verifyTitle("Pragmatic Test Store", "Homepage not loaded");
        HomePage.goToLogin();
        CommongVerifications.verifyTitle("Account Login", "Login page isn't loaded");
        LoginPage.login(email, password);
        LoginPage.verifyErrorValidationMessage("Warning: No match for E-Mail Address and/or Password.", "The expected validation error message is INCORRECT!");
    }

    @BeforeMethod
    public void setUp() {
        Browser.open("chrome");
    }

    @AfterMethod
    public void tearDown() {
        Browser.quit();
    }
}
