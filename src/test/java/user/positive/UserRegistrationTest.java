package user.positive;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.Registration;
import utils.Browser;
import utils.CommongVerifications;

public class UserRegistrationTest {

    @DataProvider
    public Object[][] testData() {
        return new String[][] {
                new String[] {"Tanya", "Dimitrova", "tania_anq@abv.bg", "0889546925", "#123456?", "#123456?", "yes", "yes"},
                new String[] {"Tanya", "Dimitrova", "tania_anq1@abv.bg", "0889546925", "#123456?", "#123456?", "no", "yes"},
                new String[] {"Tanya", "Dimitrova", "tania_anq2@abv.bg", "0889546925", "#123456?", "#123456?", "yes", "yes"},
                new String[] {"Tanya", "Dimitrova", "tania_anq3@abv.bg", "0889546925", "#123456?", "#123456?", "no", "yes"},
                new String[] {"Tanya", "Dimitrova", "tania_anq4@abv.bg", "0889546925", "#123456?", "#123456?", "yes", "yes"},
        };
    }

    @BeforeMethod
    public void setUp() {
        Browser.open("chrome");
    }

    @Test
    public void userRegistrationTest() {
        HomePage.openHomePage();
        CommongVerifications.verifyTitle("Pragmatic Test Store", "Homepage not loaded");
        HomePage.goToMyAccountRegistration();
        CommongVerifications.verifyTitle("Register Account", "Register page not loaded");
        Registration.enterRegistrationDetails("Tanya", "Dimitrova", "tania_anq2@abv.bg", "0889546925", "#123456?",
                "#123456?", true, true);
        CommongVerifications.verifyTitle("Your Account Has Been Created!", "Registration failed");

    }

    @Test(dataProvider = "testData")
    public void userRegistrationTestDDT(String firstName, String lastName, String email, String telephone, String password, String confirmPassword,
                                        String subscribeForNewsletter, String termsAndConditions) {
        boolean subscribed, terms;
        subscribed = subscribeForNewsletter.equalsIgnoreCase("yes");
        terms = termsAndConditions.equalsIgnoreCase("yes");
        HomePage.openHomePage();
        CommongVerifications.verifyTitle("Pragmatic Test Store", "Homepage not loaded");
        HomePage.goToMyAccountRegistration();
        CommongVerifications.verifyTitle("Register Account", "Register page not loaded");
        Registration.enterRegistrationDetails(firstName, lastName, email, telephone, password, confirmPassword, subscribed, terms);
        CommongVerifications.verifyTitle("Your Account Has Been Created!", "Registration failed");

    }


    @AfterMethod
    public void tearDown() {
        Browser.quit();
    }

}
