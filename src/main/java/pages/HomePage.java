package pages;

import org.openqa.selenium.By;
import utils.Browser;

public class HomePage {

    private static final By MY_ACCOUNT_LINK = By.xpath("//div[@id='top-links']//i[@class='fa fa-user']/../span[1]");
    private static final By MY_ACCOUNT_REGISTRATION_LINK = By.xpath("//a[@href='http://shop.pragmatic.bg/index.php?route=account/register']");
    private static final By MY_ACCOUNT_LOGIN_LINK = By.xpath("//a[@href='http://shop.pragmatic.bg/index.php?route=account/login']");


    public static void openHomePage() {
        Browser.driver.get("http://shop.pragmatic.bg/");
    }

    public static void goToMyAccountRegistration() {
        Browser.driver.findElement(MY_ACCOUNT_LINK).click();
        Browser.driver.findElement(MY_ACCOUNT_REGISTRATION_LINK).click();
    }

    public static void goToLogin() {
        Browser.driver.findElement(MY_ACCOUNT_LINK).click();
        Browser.driver.findElement(MY_ACCOUNT_LOGIN_LINK).click();
    }
}
