package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.Browser;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class Registration {

         private static final By FIRST_NAME = By.id("input-firstname");
         private static final By LAST_NAME = By.id("input-lastname");
         private static final By EMAIL = By.id("input-email");
         private static final By TELEPHONE = By.id("input-telephone");
         private static final By PASSWORD = By.id("input-password");
         private static final By CONFIRM_PASSWORD = By.id("input-confirm");
         private static final By RADIO_BUTTON = By.xpath("//div[@class='form-group']//input[@type='radio']");
         private static final By CHECKBOX = By.xpath("//input[@name='agree']");
         private static final By CONTINUE_BUTTON = By.xpath("//input[@class='btn btn-primary']");



    public static void enterRegistrationDetails(String firstName, String lastName, String email, String telephone, String password, String confirmPassword,
                                                 boolean subscribeForNewsletter, boolean termsAndConditions) {
        Browser.driver.findElement(FIRST_NAME).sendKeys(firstName);
        Browser.driver.findElement(LAST_NAME).sendKeys(lastName);
        Browser.driver.findElement(EMAIL).sendKeys(email);
        Browser.driver.findElement(TELEPHONE).sendKeys(telephone);
        Browser.driver.findElement(PASSWORD).sendKeys(password);
        Browser.driver.findElement(CONFIRM_PASSWORD).sendKeys(confirmPassword);
        List<WebElement> radioButtons = Browser.driver.findElements(RADIO_BUTTON);
        if (subscribeForNewsletter) {
            for (WebElement element: radioButtons) {
                if (element.getAttribute("value").equals("1")) {
                    if (!element.isSelected())
                        element.click();
                    assertTrue(element.isSelected());
                    break;
                }
            }
        }
        WebElement checkbox = Browser.driver.findElement(CHECKBOX);
        if(termsAndConditions) {
            if (!checkbox.isSelected()) {
                checkbox.click();
            }

        } else {
            if (checkbox.isSelected()) {
                checkbox.click();
            }
        }
        Browser.driver.findElement(CONTINUE_BUTTON).click();


    }
}
