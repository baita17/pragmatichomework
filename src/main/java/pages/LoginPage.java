package pages;

import org.openqa.selenium.By;
import org.testng.Assert;
import utils.Browser;

public class LoginPage {

    private static final By LOGIN_EMAIL =By.id("input-email");
    private static final By LOGIN_PASSWORD =By.id("input-password");
    private static final By LOGIN_BUTTON =By.xpath("//input[@class='btn btn-primary']");
    private static final By LOC_ERROR_VALIDATION_MESSAGE =By.xpath("//div[@id='account-login']//div[@class='alert alert-danger alert-dismissible']");

    public static void login(String email, String password) {
        Browser.driver.findElement(LOGIN_EMAIL).sendKeys(email);
        Browser.driver.findElement(LOGIN_PASSWORD).sendKeys(password);
        Browser.driver.findElement(LOGIN_BUTTON).click();
    }

    public static void verifyErrorValidationMessage(String expectedMessage, String messageInCaseOfIncorrectValidationMessage) {
        String actualErrorValidationMessage = Browser.driver.findElement(LOC_ERROR_VALIDATION_MESSAGE).getText();

        Assert.assertTrue(actualErrorValidationMessage.contains(expectedMessage), messageInCaseOfIncorrectValidationMessage);
    }


}
